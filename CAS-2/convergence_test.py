# Calculating some convergent series.

import math

#  Calculating Euler's constant by the Taylor series (More readable version)

v = 0.0                     # convergence value
i = 0                       # control variable for the loop
n = 100                     # iterations number for the loop
for i in range(0,n):
    v = v + 1 / math.factorial(i)

print('e = ',v)
print()

# Calculating Euler's constant by the Taylor series (Faster version)

v = 1.0                     # convergence value
i = 0                       # control variable for the loop
n = 10                    # iterations number for the loop
fat = 1                     # factorial
for i in range(1,n):
    fat = fat * i
    v = v + 1 / fat
    
print('e = ',v)
print()


# Calculating PI/4 by the Leibniz series 

n = 1000000                     # iterations number for the loop
v = 0.0                         # convergence value
i = 0                           # control variable for the loop
sign = 1                        # Controls signal switching in series
for i in range(1,n,2):
  v = v + sign * (1 / i)
  sign = -sign

print('pi/4  = ',v)
print('pi  = ', v * 4)
print()

# Reciprocal Fibonacci constant (Using the Binet Fórmula --- formula to find the nth term of fibonacci series.) 

n = 1000                        # iterations number for the loop
v = 0.0                         # convergence value
i = 0                           # control variable for the loop
fib = 1.0                       # Calculate fibonacci number by the index
for i in range(1,n):
  fib = round((pow((1+math.sqrt(5))/2,i)-pow((1-math.sqrt(5))/2,i))/ math.sqrt(5))
  v = v + (1 / fib)
  
print('fi  = ',v)
print()

# Reciprocal Fibonacci constant (Faster version)

n = 1000                        # iterations number for the loop
i = 0                           # control variable for the loop
v = 0.0                         # convergence value
fib = 0.0                       # Fibonacci number
last = 1.0                      # auxiliar var for fibonacci calculation
for i in range(1,n):
  fib,last = last,fib + last
  v = v + (1 / fib)
  
print('fi  = ',v)
print()

# Sum of Geometric progression 
n = 999999999999                                    # iterations number for the loop
v = 0.0                                             # convergence value
i = 10                                              # control variable for the loop

while (i < n):
  v = v + (9 / i)
  i = i * 10
  
print('gp  = ',v)
print()


# end